# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.9.1] - 2024-11-01

### Changed
- updated BLAKE3 to 1.5.4

### Fixed
- fix build on armle
- sundry minor bugfixes

## [0.9.0] - 2024-10-29

The release adds the new Router API, the socket OIL filter, and a whole bunch of
other new API calls, fixes and improvements.

This work was funded by NLnet through NGI Assure. NGI Assure is made possible
with financial support from the European Commission's Next Generation Internet
programme, under the aegis of DG Communications Networks, Content and
Technology. This project has received funding from the European Union’s Horizon
2020 research and innovation programme under grant agreement No 957073.

Also, we'd like to take this opportunity to thank Robert Martinez (mray) for our
shiny new Librecast Logo!

### Added
- Router API
    - lc_router_new()
    - lc_router_free()
    - lc_router_socket_add()
    - lc_router_socket_del()
    - lc_router_net() - create connected routers
    - lc_router_start()
    - lc_router_stop()
    - lc_router_socket_get()
    - lc_router_socket_set()
    - lc_router_port_set()
    - lc_router_port_unset()
    - lc_router_port_down()
    - lc_router_port_up()
    - lc_router_port_connect()
    - lc_router_port_disconnect()
    - lc_router_channel_bind()
    - lc_router_channel_unbind()
    - lc_router_lock()
    - lc_router_trylock()
    - lc_router_unlock()
    - lc_router_lockpair()
    - lc_router_onready() - router callback
    - lc_router_acquire() - router readlock
    - lc_router_release()
- Router Topologies:
    - LC_TOPO_NONE - do not connect anything
    - LC_TOPO_CHAIN - routers are daisy-chained
    - LC_TOPO_RING - ring topology
    - LC_TOPO_TREE - loop-free spanning tree
    - LC_TOPO_STAR - root router is connected to all others
    - LC_TOPO_MESH - full mesh (all routers directly connected)
    - LC_TOPO_RANDOM - surprise!
    - LC_TOPO_BUTTERFLY - a fixed topology of 6 routers, commonly used in network coding examples
    - LC_TOPO_HONEYCOMB - creates a network of 3-port routers with a ring of 3 routers redundantly connected to n hexagon rings.
- router speedtest
- lc_channel_recvmsg()
- track socket bytes sent
- lc_socket_by_fd() - find socket by file descriptor
- Add RaptorQ encoding support to lc_channel_sendmsg()
- channel read locking
    - lc_channel_acquire()
    - lc_channel_release()
- socket read locking
    - lc_socket_acquire()
    - lc_socket_release()
- context read locking
    - lc_ctx_acquire()
    - lc_ctx_release()
- lc_socket_recvmmsg() - receive multiple messages on a Librecast socket
- make test/tsan (testing with -fsanitize=thread)
- Context queue and worker threads
    - lc_ctx_queue_init()
    - lc_ctx_qpool_resize()
    - lc_ctx_queue_free()
    - lc_ctx_q()
- lc_channel_socket_by_hash()
- lc_channel_copy() - perform full copy of channel
- Add lc_socketpair() - create a pair of connected sockets.
- Socket OIL filter
- Add socket OIL functions to API
    - lc_socket_oil_add()
    - lc_socket_oil_del()
- Tracking of sending stats in lc_share()
- various additional configure checks

### Changed
- New Librecast Logo contributed by Robert Martinez (mray).
- channel NACK logging: access structures atomically
- defer socket close until lc_socket_release()
- make memcheck: ignore "still reachable"
- update BLAKE3 -> 1.5.1
- store full channel hash

### Fixed
- support lcrq 0.0.1: conditionally compile OTI
- Fix compiling without lcrq
- make testloop: clean logs between test runs
- thread safety improvements, fixing various data races
- lc_socket_send() do not return error for 0 bytes
- ensure ENOKEY is defined on non-Linux
- fix leaks in tests 0047, 0064
- ensure socket thread is cancelled when socket is closed
- various other minor bugfixes

### Removed
- configure: remove GNU malloc macros
- remove unused function lc_hashgroup()
- removed unused uri field from lc_channel_t

## [0.8.0] - 2023-11-07

This release adds recursive directory tree syncing, along with additional
debugging output and other bugfixes.

### Added

- recursive directory syncing
- mdex_sharepath() - return the network sharepath
- mdexing and syncing of symlinks
- make test/zzz - slow tests, not run as part of main test suite
- context debugging functions
    - lc_ctx_debug() - set debug flags
    - lc_ctx_stream() - set debug message output stream
- debug settings for printing files during mdexing
    - If mdex->debug has the MDEX_DEBUG_FILE flag set, then file names will be
    printed to mdex->stream during indexing
    - keep track of number of entries (files, directories) indexed

## Changed

- lc_syncfilelocal: set file metadata

### Fixed

- correctly handle mdexing and syncing of zero length files
- doc: mdex_add(3) - correction
- fix function declarations with no prototype (void)
- lc_share: ensure errno is set on error
- mld_start: set errno if LISTEN thread fails

## [0.7.0] - 2023-08-22

This release adds the file syncing, sharing, mdex, mtree and smolq APIs, along
with various other new API calls and improvements.

### Added

- sync API: file and data syncing API
- smolq: small queue API
- mdex: channel Indexing API - mdex_*()
- mtree: merkle tree hashing API
- lc_ctx_ifx() - set default interface for sockets and channels created with context
- lc_ctx_ratelimit() - set ratelimit on context
- lc_channel_ratelimit(): channel ratelimiting (sending)
- lc_share() / lc_unshare() - multicast file sharing API
- lc_channel_oti_peek()
- LC_CODE_FEC_OTI - send/recv RaptorQ FEC Object Transmission Information headers
- lc_channel_rq()
- lc_hashtoaddr() - create IPv6 multicast addr from supplied hash and flags
- context encoding functions
    - lc_ctx_coding_set()
    - lc_ctx_getkey()
    - lc_ctx_setkey()
    - lc_ctx_set_sym_key()
    - lc_ctx_set_pub_key()
- man pages for various API calls
- check for network before running network tests
- packet timeout in lc_channel_recv_lcrq
- MSG_DROP flag for testing

### Changed
- default to configure --with-mld
- use more portable __attribute__ syntax
- improved test runner + logging
- improved BPF filter for MLD packets
- randomize RaptorQ repair symbols
- send additional RaptorQ repair symbols (2x overhead)

### Fixed
- lc_ctx_keygetorset, lc_channel_keygetorset - set errno on error
- preserve errno in our various free functions
- ifdef guard rq_free
- lc_channel_recv() - calculate RaptorQ ESI correctly
- various Makefile fixes
- various configure fixes
- various test fixes
- libmld: limit BPF filter to icmp6 and ensure mrecs is a reasonable number
- removed ineffective channel mlocks when setting encryption keys. These need to
  be done by the calling program or implemented in a separate API call.
- test 0000-0044 - fix race condition
- improved error handling in libmld
- fix race in lc_channel_send_coded()
- fixes to lc_channel_recv() RaptorQ decoding
- fix sodium_bin2hex definition when libsodium not enabled
- mld_state_grp_check(): set ENODEV
- lc_socket_recv(), lc_socket_recvmsg() - set errno to EBADMSG on decoding failure
- libmld: only set SO_REUSEPORT if defined
- libmld: new BPF filter for MLD packets - filter some unwanted packets
- libmld: mld_query_msg(): free cmsgbuf on error
- replace RaptorQ repair symbols with original when received
- fix buffer overrun when decoding
- fix race in lc_channel_send_coded()
- libmld: fix race in mld_start()
- libmld: avoid bind() of mld->sock to avoid setsockopt failures
- libmld: mld_start(): wait for netlink thread to be ready
- libmld: error checking in mld_listen (BSD version)
- define byteorder macros for macOS/Darwin

## Removed
- lc_ctx_get_id()
- lc_socket_get_id()
- lc_channel_get_id()

## [0.6.1] - 2023-05-14

### Fixed

- fix library SONAME (0.5, not 0.5.1)

## [0.6.0] - 2023-05-07

### Added
- NACK/replay API calls
	- lc_channel_check_seqno()
	- lc_channel_detect_gaps()
	- lc_channel_nack_add_log()
	- lc_channel_nack_handler()
	- lc_channel_nack_handler_thr()
- MLD API (merged in libmld)
- FreeBSD, NetBSD and OpenBSD support for tap creation

## Changed
- BLAKE3 upgraded to v1.3.3

## Fixed
- fix cross-building on incompatible architectures by directly linking ABI
  version
- fix hash_generic_key() for BLAKE3
- various test fixes, including fixes for NetBSD and FreeBSD

## [0.5.1] - 2022-07-16

### Fixed
- CID 274982 Unchecked return value from library - check return from setsockopt()
- CID 274984 Unchecked return value from library - check return from setsockopt()
- CID 274983 Resource leak - free() on error path
- fix fatal build bug ("missing" config.h)
- blake3: disable NEON on arm64
- install: fix library symlink creation
- minor test fixes

## [0.5.0] - 2022-07-14

### Added
- RaptorQ (RFC 6330) Forwards Error Correction
- symmetric key message encryption
- channel key management/encoding functions
	- lc_channel_setkey()
	- lc_channel_getkey()
	- lc_channel_set_sym_key()
	- lc_channel_set_pub_key()
	- lc_channel_coding_set
- lc_channel_init_grp() - convenience function for calling lc_channel_init()
- make net-setup, net-teardown, testshell targets for testing
- configure + autotools build system
- man lc_channel_coding_set(3)
- define macro for crypto_secretbox_keygen() for libsodium < 1.0.12

### Changed
- Makefile changes, cleanup
- various documentation updates

### Fixed
- Gave Up Github (https://sfconservancy.org/GiveUpGitHub/). Hello Codeberg!
- removed hardcoded bash path (was breaking tests on NixOS)
- respect LDFLAGS and CPPFLAGS for hardening flags (#35)
- test runner for {Free,Net}BSD - create required symlink
- various build & test fixes on different systems

## [0.4.5] - 2022-04-04

### Added
- lc_tuntap_create() - create TUN/TAP sockets
- lc_channel_random() - create random channel
- tracking group joins per socket when IPV6_MULTICAST_ALL not defined

This means ALL packets for ALL multicast groups joined by ANY PROCESS owned by ANY USER will be received by a socket by default. That's ... surprising. And not the behaviour we want.

Librecast needs to track group joins per socket and drop any packets that aren't expected on that socket.

- added Repology badge to README

### Changed
- don't force clean before tests
- use newer SIOCBRDELIF in bridge code
- explicitly include <linux/sockios.h> to ensure SIOCBRDELIF defined

### Fixed

- use non-default channel port if specified on recv
- check for invalid opcodes before calling message handler
- copy ancillary pkt info to ensure memory is aligned
- fix test 20 on big endian
- zero memory for device name array in test 32
- add missing <net/if.h> header

## [0.4.4] - 2021-06-05

### Added
- lc_bridge_add() / lc_bridge_del()
- lc_tap_create()
- lc_link_set() - bring up / tear down network interfaces
- lc_bridge_addif() / lc_bridge_delif()
- fallback interface code for unsupported platforms
- lc_channel_send() / lc_socket_recv() - raw channel/socket send/recv functions
- lc_channel_sendmsg() / lc_socket_recvmsg()
- lc_socket_bind() - join on all multicast-capable interfaces, or bound socket ifx
- lc_socket_send() - send to all channels bound to socket
- lc_socket_sendmsg()
- lc_socket_ttl() - set socket TTL

### Changed

- License changed to GPL-2.0 or GPL-3.0 (dual licenced)
- Update README - irc channel moved to Libera.chat
- Default hashing function changed to BLAKE2B from libsodium
- libs/Makefile: Fix targets when building without blake3
- split bridge/interface code by O/S
- remove -std=gnu99 from NetBSD build - required for NetBSD 7, no longer reqd for 9.

### Fixed
- lc_msg_recv(): add cancellation point before recvmsg() - hangs on NetBSD without this.
- lc_channel_bind(): set SO_REUSEPORT if defined - required on NetBSD to prevent "address
    already in use" errors.

## [0.4.3] - 2021-03-09

### Fixed

- Use IPV6_JOIN_GROUP / IPV6_LEAVE_GROUP in preference to obsolete IPV6_ADD_MEMBERSHIP / IPV6_DROP_MEMBERSHIP
- Fix tempfile creation for tests on NetBSD.
- Sort uses of "wildcard" in Makefile to make ordering of files predictible and avoid potential reproducibility issues.
- Makefile fixes for NetBSD - replace call to ldconfig

## [0.4.2] - 2021-03-06

### Added

`<librecast/crypto.h>`
- hash_generic()
- hash_generic_key()
- hash_init()
- hash_update()
- hash_final()
- hash_hex_debug()
- hash_bin2hex()

### Changed

- Changed default hashing function to BLAKE3. This is faster and and has similar
    security properties to BLAKE2B from libsodium.  Build with `make USE_LIBSODIUM=1` to
    use BLAKE2B instead.

### Fixed

- Support DESTDIR when installing docs.
- Pass LIBDIR to ldconfig in the install target.
- Building without libsodium.
- Ensure a clean build before running single tests.
- Work around bugs in gcc and glibc to fix test malloc

## [0.4.1] - 2021-03-04

### Added
- Instructions for Ubuntu to install prerequisite libsodium-dev (Esther Payne)

### Fixed
- Remove references to obsolete libraries in test Makefile (Esther Payne)

## [0.4.0] - 2021-03-04

### Added
- CHANGELOG.md (this file)
- test/falloc.c - failing malloc checker so we can force memory allocation
    failures in testing.
- libsodium dependency (required for hashing)
- valgrind.h added to `test/` so we can skip tests that don't play nicely with
    valgrind.

### Changed
- The base multicast networking API has been reviewed, extensively refactored and simplified.
- Functions were reordered more logically, grouping functions that call each
    other close together to improve efficiency.
- Network interface indexes are now unsigned values everywhere.
- Changes to the Channels API. lc_channel_init() now takes sockaddr_in6 so
    address and port can be directly specified and to save much converting back
    and forth between string and binary addresses. All calls to getaddrinfo()
    and use of struct addrinfo have been removed - there's really no need for
    this in multicast code.
- Sockets and Channels are now inserted at the head of their lists. This is
    quicker, simplifies the code, and makes finding the most recently added faster.
- SHA1 hash replaced with BLAKE2B from libsodium.
- Renumbered error codes as negative.
- lc_msg_logger() - optional message logging implemented as a function pointer

### Removed
- The experimental database API has been removed completely for now, as it was intertwined with the network code. The core multicast code should not be require any database functionality or dependencies. This will be rewritten in the next milestone.
- All logging has been removed. This is a library - we return error codes in serene silence and let the programmer decide what to do with them.
- Removed some pointless argument checking in various API calls. A careless
    programmer won't be checking the return codes anyway. In some cases these
    have been replaced with assert()s to catch accidental API misuse.
- OpenSSL dependency
- libbridge dependency
- Linux-specific headers
- Removed obsolete tests.

### Fixed
- docs (man pages) are now installed with `make install`


## [0.3.0] - 2020-09-05

### Added
- The code now compiles using either gcc or clang.  There is a "make clang" target in the Makefile.
- Added test runner and a set of test modules to the project to exercise
all the main functions, including common error conditions.  This will continue
to be added to as I have adopted test driven development for the project.

`make test` runs all the tests.
`make check` runs all the tests using valgrind to do leak checking and dynamic
analysis.

`make 0000-0004.test` runs a single test.
`make 0000-0004.check` runs a single test with valgrind
`make 0000-0004.debug` runs a single test with the gdb debugger
`make sparse` compiles the project using cgcc (the sparse static analyser)
`make clang` builds the project using clang
`make coverity` builds the project using the coverity static analyser, creating
a librecast.tgz ready to upload to Coverity Scan for analysis.

### Changed

- Split the library into three separate parts and removed some redundant code.
- There are now separate headers and shared libraries for IPv6 Multicast messaging (net), local database commands and querying (lsdb) and remote (multicast) database commands (lcdb)

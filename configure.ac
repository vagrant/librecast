#                                               -*- Autoconf -*-
# Process this file with autoconf to produce a configure script.

AC_PREREQ([2.71])
AC_INIT([librecast], [0.9.1], [bugs@librecast.net])
AC_SUBST(PACKAGE_ABIVERS, 0.6)
AC_COPYRIGHT(Copyright (c) 2016-2024 Brett Sheffield <bacs@librecast.net>
See COPYING for license details.
)
AC_CONFIG_SRCDIR([src/])
AC_CONFIG_HEADERS([src/config.h])

# Checks for programs.
AC_PROG_CC
AC_PROG_INSTALL
AC_PROG_LN_S
AC_CHECK_PROG([testrunner], [lctest], [lctest], [maketest])
AC_SUBST([TESTRUNNER],[$testrunner])
AC_CHECK_PROGS([FALSE], [false], [/usr/bin/env false])
AC_CHECK_PROG([ifconfig], [ifconfig], [ifconfig])
AC_SUBST([IFCONFIG],[$ifconfig])
AC_CHECK_PROG([iproute], [ip], [iproute])
AC_SUBST([IPROUTE],[$iproute])
AC_CHECK_PROG([ROUTE], [route], [route])

# Checks for libraries.
AC_CHECK_LIB([dl], [dlsym])
AC_CHECK_LIB([m], [log2])
AC_CHECK_LIB([pthread], [pthread_create])
AC_SEARCH_LIBS([arc4random_uniform], [bsd])
AC_SEARCH_LIBS([clock_gettime], [rt])
AC_SEARCH_LIBS([__atomic_load_8], [atomic])

AC_ARG_WITH([mld], AS_HELP_STRING([--without-mld], [disable mld]))
AC_ARG_WITH([blake3],
	    AS_HELP_STRING([--with-blake3], [use BLAKE3 for hashing (default is yes)])
	    )
LIBBLAKE3=
AS_IF([test "x$with_blake3" != xno],
            AC_SUBST([BLAKE3], [blake3])
            AC_SUBST([LIBBLAKE3], [-lblake3])
            AC_SUBST([INCBLAKE3], ["#include <librecast/blake3.h>"])
	    AC_DEFINE([HAVE_BLAKE3], [1], [Define to 1 to use the `blake3' library for hashing.])
)

LIBMLD=
AS_IF([test "x$with_mld" != "xno"], [
      AC_SUBST([MLD], [libmld])
      AC_SUBST([LIBMLD], [-lmld])
      AC_SUBST([OBJMLD], [../libs/libmld/src/*.o])
      AC_SUBST([MLD_HEADERS], [../libs/libmld/include/*.h])
      AC_SUBST([INCMLD], ["-I ../libs/libmld"])
      AC_DEFINE([HAVE_MLD], [1], [Define to 1 to use the MLD library.])
])

AC_ARG_WITH([sodium],
	    AS_HELP_STRING([--with-sodium], [use libsodium for encryption and \
                           hashing (default is yes, if available)]),
            [AC_SUBST([HAVE_LIBSODIUM], [])],
            [with_sodium=check])

LIBSODIUM=
AS_IF([test "x$with_sodium" != xno],
            [AC_CHECK_LIB([sodium], [sodium_init],
              [AC_SUBST([LIBSODIUM], ["-lsodium"])
               AC_SUBST([INCSODIUM], ["#include <sodium.h>"])
               AC_SUBST([HAVE_LIBSODIUM], ["HAVE_LIBSODIUM := 1"])
               AC_DEFINE([HAVE_LIBSODIUM], [1],
                         [Define to 1 if you have the `sodium' library (-lsodium).])
              ],
              [if test "x$with_sodium" != xcheck; then
                 AC_MSG_FAILURE([--with-sodium was given, but libsodium not found])
               fi
              ], -lsodium)])

AS_IF([test "x$with_blake3" != xno],
      AC_SUBST([HASH_TYPE], ["#define HASH_TYPE HASH_BLAKE3"])
      AC_SUBST([HASHSIZE], ["#define HASHSIZE BLAKE3_OUT_LEN"])
      AC_SUBST([HASH_STATE], ["typedef blake3_hasher hash_state;"])
      ,
      AS_IF([test "x$with_sodium" != xno],
	    [
	     AC_SUBST([HASH_TYPE], ["#define HASH_TYPE HASH_BLAKE2"])
	     AC_SUBST([HASHSIZE], ["#define HASHSIZE crypto_generichash_BYTES"])
             AC_SUBST([HASH_STATE], ["typedef crypto_generichash_state hash_state;"])
	     ],
	     [
	     AC_SUBST([HASH_TYPE], ["#undef HASH_TYPE"])
	     AC_SUBST([HASHSIZE], ["#undef HASHSIZE"])
	     ]
	     )
      )


AC_ARG_WITH([lcrq],
	    AS_HELP_STRING([--with-lcrq], [use liblcrq for FEC \
                           (default is yes, if available)]),
            [AC_SUBST([HAVE_LIBLCRQ], [])],
            [with_lcrq=check])

LIBLCRQ=
AS_IF([test "x$with_lcrq" != xno],
            [AC_CHECK_LIB([lcrq], [rq_init],
              [
	       AC_SUBST([LIBLCRQ], ["-llcrq"])
               AC_SUBST([HAVE_LIBLCRQ], ["HAVE_LIBLCRQ := 1"])
               AC_DEFINE([HAVE_LIBLCRQ], [1],
                         [Define to 1 if you have the `lcrq' library (-llcrq).])
              ],
              [if test "x$with_lcrq" != xcheck; then
                 AC_MSG_FAILURE([--with-lcrq was given, but liblcrq not found])
               fi
              ], -llcrq)])

AC_CHECK_LIB([lcrq], [rq_init])
AC_CHECK_FUNCS([rq_oti])

# Checks for header files.
AC_CHECK_HEADERS([arpa/inet.h fcntl.h netdb.h netinet/in.h stdint.h sys/ioctl.h sys/param.h sys/socket.h unistd.h],[],AC_MSG_ERROR([required header file missing]))
AC_CHECK_HEADERS([endian.h sys/endian.h libkern/OSByteOrder.h])
AC_CHECK_HEADERS([net/if_tap.h net/if_tun.h])
AC_CHECK_HEADERS([sys/time.h utime.h])
AC_CHECK_HEADER_STDBOOL

# Checks for typedefs, structures, and compiler characteristics.
AC_C_INLINE
AC_C_RESTRICT
AC_TYPE_INT16_T
AC_TYPE_INT32_T
AC_TYPE_INT64_T
AC_TYPE_MODE_T
AC_TYPE_OFF_T
AC_TYPE_SIZE_T
AC_TYPE_SSIZE_T
AC_TYPE_UID_T
AC_TYPE_UINT8_T
AC_TYPE_UINT16_T
AC_TYPE_UINT32_T
AC_TYPE_UINT64_T
AC_CHECK_TYPES([ptrdiff_t])

# Checks for library functions.
AC_FUNC_CHOWN
AC_FUNC_MMAP
AC_FUNC_LSTAT_FOLLOWS_SLASHED_SYMLINK
AC_CHECK_FUNCS([arc4random_uniform])
AC_CHECK_FUNCS([clock_gettime ftruncate getpagesize memset mkdir munmap pathconf realpath socket strdup strndup strerror strtoull],,AC_MSG_ERROR([required function missing]))
AC_CHECK_FUNCS([epoll_create1 kqueue poll recvmmsg])
AC_CHECK_FUNCS([utimensat utimes])
AC_CHECK_FUNCS([recvmmsg])
AC_CHECK_FUNCS([rawmemchr memchr])
AC_CHECK_FUNCS([getcwd],,AC_MSG_WARN([required test function missing. Some tests may not function]))

AC_CONFIG_FILES([Makefile
                 doc/Makefile
                 libs/Makefile
                 libs/blake3/Makefile
                 libs/blake3/c/Makefile
                 src/Makefile
                 test/Makefile
		 include/librecast/crypto.h
		 ])
if test "x$with_mld" != "xno"; then
	AC_CONFIG_SUBDIRS([libs/libmld])
fi
AC_OUTPUT

/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only */
/* Copyright (c) 2024 Brett A C Sheffield <bacs@librecast.net> */

#include "test.h"
#include <librecast/net.h>

int main(void)
{
	char name[] = "lc_socket_ctx() - return context for socket";
	lc_ctx_t *lctx, *ctx_check;
	lc_socket_t *sock;

	test_name(name);

	lctx = lc_ctx_new();
	if (!test_assert(lctx != NULL, "lc_ctx_new()")) return test_status;
	sock = lc_socket_new(lctx);
	if (!test_assert(sock != NULL, "lc_socket_new()")) goto err_ctx_free;
	ctx_check = lc_socket_ctx(sock);
	test_assert(ctx_check == lctx, "lc_socket_ctx() returns ctx");

err_ctx_free:
	lc_ctx_free(lctx);
	return test_status;
}

/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only */
/* Copyright (c) 2023-2024 Brett Sheffield <bacs@librecast.net> */

#include "test.h"
#include "testdata.h"
#include <librecast/mdex.h>
#include <librecast.h>
#include <librecast/crypto.h>

static mdex_stack_t stack;

#include <mdex.c>

int main(void)
{
	void * testptr = (void *)0xcafe;
	void * ret;
	test_name("mdex_stack_push() / mdex_stack_pop()");

	test_assert(mdex_stack_pop(&stack) == NULL,
			"mdex_stack_pop() with empty stack => NULL");

	/* push some entries onto the stack */
	for (int i = 0; i < MDEX_STACK_SZ; i++) {
		mdex_stack_push(&stack, (char *)testptr + i);
	}

	/* pop them back again */
	for (int i = MDEX_STACK_SZ - 1; i > -1; i--) {
		ret = mdex_stack_pop(&stack);
		test_assert(ret == (char *)testptr + i, "push and pop returned %p", ret);
	}

	test_assert(mdex_stack_pop(&stack) == NULL,
			"mdex_stack_pop() with empty stack => NULL");

	free(stack.stack);
	return test_status;
}

/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only */
/* Copyright (c) 2023 Brett Sheffield <bacs@librecast.net> */

#include "test.h"
#include "testdata.h"
#include <librecast/mdex.h>
#include <librecast.h>
#include <librecast/crypto.h>

int main(void)
{
	mdex_t *mdex;
	unsigned char rand[123];
	unsigned char hash[HASHSIZE];
	int rc;

	test_name("mdex_*() return EINVAL when arguments invalid");

	test_random_bytes(rand, sizeof rand);
	hash_generic(hash, HASHSIZE, rand, sizeof rand);

	/* mdex required for put/get/del */

	errno = 0;
	rc = mdex_put(NULL, hash, HASHSIZE, NULL);
	test_assert(rc == -1, "mdex_put() requires mdex_t");
	test_assert(errno == EINVAL, "errno == EINVAL");

	errno = 0;
	rc = mdex_get(NULL, hash, HASHSIZE, NULL);
	test_assert(rc == -1, "mdex_get() requires mdex_t");
	test_assert(errno == EINVAL, "errno == EINVAL");

	errno = 0;
	rc = mdex_del(NULL, hash, HASHSIZE);
	test_assert(rc == -1, "mdex_del() requires mdex_t");
	test_assert(errno == EINVAL, "errno == EINVAL");

	mdex = mdex_init(1);

	/* hash and hashlen > 0 required for put/get/del */

	errno = 0;
	rc = mdex_put(mdex, NULL, HASHSIZE, NULL);
	test_assert(rc == -1, "mdex_put() requires hash");
	errno = 0;
	rc = mdex_put(mdex, hash, 0, NULL);
	test_assert(rc == -1, "mdex_put() requires hashlen");

	errno = 0;
	rc = mdex_get(mdex, NULL, HASHSIZE, NULL);
	test_assert(rc == -1, "mdex_get() requires hash");
	errno = 0;
	rc = mdex_get(mdex, hash, 0, NULL);
	test_assert(rc == -1, "mdex_get() requires hashlen");

	errno = 0;
	rc = mdex_del(mdex, NULL, HASHSIZE);
	test_assert(rc == -1, "mdex_del() requires hash");
	errno = 0;
	rc = mdex_del(mdex, hash, 0);
	test_assert(rc == -1, "mdex_del() requires hashlen");

	mdex_free(mdex);
	return test_status;
}

/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only */
/* Copyright (c) 2023 Brett Sheffield <bacs@librecast.net> */

/* test canonpath()
 *
 * canonpath() is the same as glibc's realpath(3), except it does not resolve
 * symlinks
 */

#include "test.h"
#include "testdata.h"
#include <canonpath.h>

int main(int argc, char *argv[])
{
	(void)argc;
	char *src, *csrc, *rsrc;

	test_name("canonpath()");

	test_createtestdir(basename(argv[0]), &src, "src");
	csrc = canonpath(src);
	rsrc = realpath(src, NULL);
	test_log("src:  '%s'\n", src);
	test_log("csrc: '%s'\n", csrc);
	test_log("rsrc: '%s'\n", rsrc);
	test_assert(csrc != NULL, "canonpath() did not return NULL");
	test_assert(!strcmp(csrc, rsrc), "canonpath() == realpath()");
	free(rsrc);
	free(csrc);
	free(src);

	return test_status;
}

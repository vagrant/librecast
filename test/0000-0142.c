/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only */
/* Copyright (c) 2023 Brett Sheffield <bacs@librecast.net> */

/* test 0142
 * NULL mtree - build an mtree of a zero-length object
 */

#include "test.h"
#include "testdata.h"
#include <librecast/mtree.h>
#include <errno.h>

#define HEXDUMP

int main(void)
{
	unsigned char hash[HASHSIZE] = "";
	unsigned char *root;
	char *data = NULL;
	mtree_t tree = {0};
	int rc;

	test_name("mtree_build() - zero length data");

	rc = mtree_init(&tree, 0);
	test_assert(rc == 0, "mtree_init()");

	rc = mtree_build(&tree, data, NULL);
	test_assert(rc == 0, "mtree_build()");

#ifdef HEXDUMP
	mtree_hexdump(&tree, stderr);
#endif

	rc = mtree_verify(&tree);
	test_assert(rc == 0, "mtree_verify()");

	/* fetch root node, ensure it is all zeros */
	root = mtree_nnode(&tree, 0);
	if (!test_assert(root != NULL, "mtree_nnode() returned root")) goto err_free_tree;
	test_assert(!memcmp(root, hash, HASHSIZE), "root hash is all zeros");

	/* ensure max and min functions return 0 (root) */
	test_assert(mtree_subtree_data_min(tree.base, 0) == 0,
			"mtree_subtree_data_min(base, 0) is zero");
	test_assert(mtree_subtree_data_max(tree.base, 0) == 0,
			"mtree_subtree_data_max(base, 0) is zero");

err_free_tree:
	mtree_free(&tree);
	return test_status;
}

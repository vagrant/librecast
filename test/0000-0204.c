/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only */
/* Copyright (c) 2024 Brett Sheffield <bacs@librecast.net> */

#include "test.h"
#include <librecast_pvt.h>
#include <librecast/router.h>

#define ROUTERS 6

int main(void)
{
	lc_ctx_t *lctx;
	lc_router_t *r[ROUTERS];
	int rc;

	test_name("lc_router_net() - LC_TOPO_NONE");

	lctx = lc_ctx_new();
	if (!test_assert(lctx != NULL, "lc_ctx_new()")) return test_status;

	/* create some disconnected routers */
	rc = lc_router_net(lctx, r, ROUTERS, LC_TOPO_NONE, 0, LC_ROUTER_FLAG_FIXED);
	if (!test_assert(rc == 0, "lc_router_net()")) goto err_ctx_free;
	for (int i = 0; i < ROUTERS; i++) {
		test_log("====== ROUTER %i\n", i);
		if (!test_assert(r[i] != NULL, "r[%i] allocated", i))
			goto err_ctx_free;
		if (!test_assert(r[i]->ctx == lctx, "context set"))
			goto err_ctx_free;
		if (!test_assert(r[i]->flags == LC_ROUTER_FLAG_FIXED, "flags set"))
			goto err_ctx_free;
		if (!test_assert(r[i]->ports == 0, "ports == 0"))
			goto err_ctx_free;
	}
err_ctx_free:
	lc_ctx_free(lctx);
	return test_status;
}

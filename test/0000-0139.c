/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only */
/* Copyright (c) 2021 Brett Sheffield <bacs@librecast.net> */

#include "test.h"
#include "testdata.h"
#include <librecast/mtree.h>
#include <errno.h>

#ifdef TEST_STRESS
# define TEST_SIZE 1073741825
#else
# define TEST_SIZE 209715110
#endif
/* nice powers of two hide bugs */
static_assert(__builtin_popcount(TEST_SIZE) > 1, "TEST_SIZE must not be a power of 2");

int main(void)
{
	char *data;
	mtree_t tree = {0};
	int rc;

	test_name("mtree_build()");

	data = malloc(TEST_SIZE);
	assert(data);

	test_random_bytes(data, TEST_SIZE);

	rc = mtree_init(&tree, TEST_SIZE);
	test_assert(rc == 0, "mtree_init()");
	test_assert(tree.base >= tree.chunks, "base(%zu) > chunks(%zu)", tree.base, tree.chunks);
	if (rc == -1) goto exit_0;

	rc = mtree_build(&tree, data, NULL);
	test_assert(rc == 0, "mtree_build()");
#ifdef HEXDUMP
	mtree_hexdump(&tree, stderr);
#endif

	mtree_free(&tree);
exit_0:
	free(data);
	return test_status;
}

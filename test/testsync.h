/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only */
/* Copyright (c) 2023 Brett Sheffield <bacs@librecast.net> */

#ifndef _TESTSYNC_H
#define _TESTSYNC_H 1

/* check mode, owner, group, times */
int statcmp(const char *src, const char *dst);

/* verify src and dst match (recursive directory compare) */
void test_verify_dirs(const char *src, const char *dst);

#endif /* _TESTSYNC_H */

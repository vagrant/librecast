/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only */
/* Copyright (c) 2023 Brett Sheffield <bacs@librecast.net> */

/* canonpath() is the same as glibc's realpath(3),
 * except it does not resolve symlinks. */
char *canonpath(const char *name);

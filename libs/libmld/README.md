# libmld - IPv6 Muliticast Listener Discovery (MLD2) library

```
./configure
make
make test # NB: some tests require root/CAP_NET_RAW
```

## Bloom Filters (experimental)

NB: this option has been temporarily removed.

To enable the experimental SIMD bloom filters call configure with the
appropriate option before building:

`./configure --enable-bloom-filters`

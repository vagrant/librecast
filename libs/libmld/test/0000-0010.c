/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only */
/* Copyright (c) 2023 Brett Sheffield <bacs@librecast.net> */

#include "testnet.h"
#include "testdata.h"

#define NTHREADS 2
#define MAXJOBS 256

void *thread_job(void *arg)
{
	char buf[BUFSIZ];
	test_random_bytes(buf, sizeof buf);
	return arg;
}

int main(void)
{
	job_queue_t *q;
	job_t *job[MAXJOBS] = {0};

	test_name("job tests");

	q = job_queue_create(NTHREADS);
	test_assert(q != NULL, "job_queue_create(%i)", NTHREADS);

	for (int i = 0; i < MAXJOBS; i++) {
		job[i] = job_push_new(q, thread_job, NULL, 0, NULL, 0);
		test_assert(job[i] != NULL, "job[%i] created", i);
		if (!job[i]) { perror("job_push_new"); break; }
	}

	job_queue_destroy(q);

	return test_status;
}

# README - libs

libmld is merged as a subtree. See
[https://nuclearsquid.com/writings/subtree-merging-and-you/]
and
[https://www.atlassian.com/git/tutorials/git-subtree]

## Updating libmld

To start from a fresh clone:

```
git clone git@codeberg.org:librecast/librecast.git
cd librecast
git remote add -f libmld git@codeberg.org:librecast/libmld.git
git pull -s subtree libmld main
```

Once the remote is set up, to update:

`git subtree pull --prefix libs/libmld libmld main --squash`

This will create a squashed commit with a commit message listing the new upstream commits + a merge commit.

NB: if local commits to the subtree have been made and pushed back upstream,
doing this will try to pull these commits in again, resulting in conflicts. This
can be avoided by editing the local commit and adding something like:

```
git-subtree-dir: libs/libmld
git-subtree-split: dc7d0af181fb954b2a9a313a9e65c9729b2e5cc4
```
where git-subtree-split points to the upstream commit hash.

## Pushing a local change to libmld back upstream

`git subtree push --prefix=libs/libmld libmld main`

## subtree split

The exact history of the subtree can be split out again, and all commit hashes
will match the subproject.

`git subtree split -P libs/libmld`

This will output a single commit hash, which will be the latest commit in the
subtree project.

librecast (0.9.1-1) unstable; urgency=medium

  * New upstream release.
  * debian/copyright: Update for 0.9.1.
  * Apply patch from upstream marking a test as requiring network.
  * debian/tests/control: Mark additional tests which require network.
  * debian/tests/control: Move tests 0039 0040 0060 from flaky to regular
    tests.

 -- Vagrant Cascadian <vagrant@debian.org>  Fri, 01 Nov 2024 15:38:13 -0700

librecast (0.9.0-2) unstable; urgency=medium

  * Upload to unstable.

 -- Vagrant Cascadian <vagrant@debian.org>  Wed, 30 Oct 2024 12:08:22 -0700

librecast (0.9.0-1) experimental; urgency=medium

  * New upstream release.
  * Update to 0.6 ABI.

 -- Vagrant Cascadian <vagrant@debian.org>  Tue, 29 Oct 2024 13:00:30 -0700

librecast (0.8.0-2) unstable; urgency=medium

  * debian/control: Revert inappropriate t64 changes Breaks/Replaces on
    old liblibrecast0.5 versions.
  * debian/control: Update Standards Version to 4.7.0.

 -- Vagrant Cascadian <vagrant@debian.org>  Wed, 10 Apr 2024 18:26:20 -0700

librecast (0.8.0-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Rename libraries for 64-bit time_t transition.  Closes: #1062605

 -- Benjamin Drung <bdrung@debian.org>  Wed, 28 Feb 2024 17:15:07 +0000

librecast (0.8.0-1) unstable; urgency=medium

  * New upstream release.
  * debian/rules: Remove additional cruft in dh_auto_clean override.
    (Closes: #1048038)

 -- Vagrant Cascadian <vagrant@debian.org>  Fri, 10 Nov 2023 11:49:06 -0800

librecast (0.7.0-1) unstable; urgency=medium

  * New upstream release.

 -- Vagrant Cascadian <vagrant@debian.org>  Tue, 22 Aug 2023 10:39:24 -0700

librecast (0.7~rc6-1) experimental; urgency=medium

  * New release candidate.
  * debian/control: Drop iproute2 from Build-Depends to avoid performing
    network tests.
  * debian/tests/control: Add iproute2 to Depends.
  * debian/patches: Remove patch for test "0110", applied upstream.
  * debian/control: Update versioned dependency on lcrq to 0.1.1.

 -- Vagrant Cascadian <vagrant@debian.org>  Wed, 16 Aug 2023 10:22:52 -0700

librecast (0.7~rc5-1) experimental; urgency=medium

  * New release candidate.
  * debian/patches: Remove all patches, applied upstream.
  * debian/rules: Re-enable tests.
  * debian/rules: always output test logs, error on test suite failure.
  * debian/patches: Add patch from upstream fixing test.
  * debian/tests/control: Fix typo for calling test.

 -- Vagrant Cascadian <vagrant@debian.org>  Tue, 15 Aug 2023 14:01:39 -0700

librecast (0.7~rc3-3) experimental; urgency=medium

  * debian/patches: Pull more test suite fixes from upstream.
  * debian/rules: Drop debugging calls to ip.

 -- Vagrant Cascadian <vagrant@debian.org>  Sat, 12 Aug 2023 16:08:51 -0700

librecast (0.7~rc3-2) experimental; urgency=medium

  * debian/patches: Apply patches from upstream to improve tests.
  * debian/rules: output test suite log on test suite failure.
  * debian/rules: Get some crude info about the network configuration.
  * debian/control: Add iproute2 to Build-Depends.

 -- Vagrant Cascadian <vagrant@debian.org>  Wed, 09 Aug 2023 09:31:17 -0700

librecast (0.7~rc3-1) experimental; urgency=medium

  * New release candidate.
  * Adapt tests and run via autopkgtest.
  * debian/patches: Add patch from upstream to use prefix from configure.
  * debian/rules: Remove dh_auto_install override.

 -- Vagrant Cascadian <vagrant@debian.org>  Tue, 08 Aug 2023 13:31:39 -0700

librecast (0.7~rc1-1) experimental; urgency=medium

  * New release candidate.
  * debian/rules: Disable several tests that depend on networking.

 -- Vagrant Cascadian <vagrant@debian.org>  Tue, 25 Jul 2023 18:15:07 -0700

librecast (0.6.1-2) unstable; urgency=medium

  * Upload to unstable.
  * debian/control: Update Standards-Version to 4.6.2.

 -- Vagrant Cascadian <vagrant@debian.org>  Sat, 17 Jun 2023 19:48:09 -0700

librecast (0.6.1-1) experimental; urgency=medium

  * debian/watch: Handle release candidate versions.
  * debian/patches: Remove patches applied in 0.6-RC4.
  * debian/rules: Add dh_auto_configure override to pass --with-mld.
  * debian/copyright: Update for 0.6-RC4.

 -- Vagrant Cascadian <vagrant@debian.org>  Sun, 14 May 2023 10:52:42 -0700

librecast (0.5.1-4) unstable; urgency=medium

  [ Pino Toscano ]
  * Move development man pages to liblibrecast-dev: they document the API,
    so they are not of much use in the library package
    - move the liblibrecast0.4 breaks/replaces from liblibrecast0.5 to
      liblibrecast-dev
    - add breaks/replaces in liblibrecast-dev against liblibrecast0.5

 -- Vagrant Cascadian <vagrant@debian.org>  Wed, 10 Aug 2022 15:36:41 -0700

librecast (0.5.1-3) unstable; urgency=medium

  * debian/control: Set default Section to "libs".
    (Closes: #1016848)

 -- Vagrant Cascadian <vagrant@debian.org>  Tue, 09 Aug 2022 17:09:40 -0700

librecast (0.5.1-2) unstable; urgency=medium

  * debian/patches: Make test 0012 time more robust.
  * debian/rules: Output the results of running the test suite.
  * debian/control: Add Breaks and Replaces on liblibrecast0.4. Thanks to
    Axel Beckert.  (Closes: #1016565)
  * debian/patches: Directly symlink the .so ABI version file.

 -- Vagrant Cascadian <vagrant@debian.org>  Thu, 04 Aug 2022 12:54:16 -0700

librecast (0.5.1-1) experimental; urgency=medium

  * debian/patches: Remove patches applied upstream.
  * debian/control: Add Build-Depends on liblcrq-dev.
  * debian/control: Update short descriptions.

 -- Vagrant Cascadian <vagrant@debian.org>  Wed, 20 Jul 2022 13:07:13 -0700

librecast (0.5.0-1) experimental; urgency=medium

  * debian/control: Update Homepage.
  * debian/copyright, debian/watch: Update upstream URLs.
  * debian/copyright: Update for 0.5.0.
  * debian/patches: Add patches from upstream to fix build failures.
  * Update packaging for new ABI.
  * debian/control: Update Standards-Version to 4.6.1.
  * debian/patches: Drop modifications to CHANGELOG.md.

 -- Vagrant Cascadian <vagrant@debian.org>  Thu, 14 Jul 2022 13:52:28 -0700

librecast (0.4.5-1) experimental; urgency=medium

  * New upstream release.
  * debian/watch: Switch to monitoring tags.
  * debian/control: Update Standards-Version to 4.6.0, no changes.

 -- Vagrant Cascadian <vagrant@debian.org>  Wed, 06 Apr 2022 11:59:13 -0700

librecast (0.4.4-1) experimental; urgency=medium

  * New upstream release.
  * debian/copyright: Update with new licensing.
  * debian/patches: Remove patch, applied upstream.

 -- Vagrant Cascadian <vagrant@debian.org>  Fri, 11 Jun 2021 10:59:19 -0700

librecast (0.4.3-1) experimental; urgency=medium

  * New upstream release.
  * debian/control: liblibrecast-dev: Add depends on
    liblibrecast0.4. Thanks to Andreas Beckmann. (Closes: #985694)
  * Use libsodium on all architectures.
  * debian/patches: Remove patch to use blake3 in top-level directory.
  * debian/copyright: Remove blake3, no longer shipped.
  * debian/patches: Fix when building without blake3.

 -- Vagrant Cascadian <vagrant@debian.org>  Fri, 02 Apr 2021 11:57:09 -0700

librecast (0.4.2-2) experimental; urgency=medium

  * debian/control: Add Vcs-* headers.
  * Switch back to using libsodium on non-x86 architectures.
  * debian/control: Use multicast instead of multi-cast in Description.

 -- Vagrant Cascadian <vagrant@debian.org>  Mon, 08 Mar 2021 15:59:56 -0800

librecast (0.4.2-1) experimental; urgency=medium

  * Move include files into liblibrecast-dev.
  * debian/patches: Remove all patches, applied upstream.
  * debian/rules: dh_auto_test: Remove CFLAGS workaround, fixed upstream.
  * debian/control: Drop Build-Depends on libsodium-dev.
  * debian/copyright: Update for blake3.
  * debian/patches: Use blake3 in top level directory.

 -- Vagrant Cascadian <vagrant@debian.org>  Mon, 08 Mar 2021 13:49:56 -0800

librecast (0.4.1-1) experimental; urgency=low

  * Initial release.

 -- Vagrant Cascadian <vagrant@debian.org>  Fri, 05 Mar 2021 17:44:47 -0800
